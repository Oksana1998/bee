@api
Feature: Users registration
  Tests new users registration

  @api.registration
  Scenario: Registration new individual Saudi user with Absher
    Given I create saudi individual user with Absher
      | key                   | value            |
      | national_id           | 1069400750       |
      | confirmation_code     | 0000             |
      | number                | 000000000        |
      | prefix                | 966              |
      | code                  | 000000           |
      | locale                | en               |
      | login                 | random           |
    Then get notification service request to assert for "Identity created"
    And check saudi user retrieves info from Laborer
    And check saudi user was added to company retrieved from Laborer service

  @api.registration
  Scenario: Registration new individual expat user with Absher
    Given I create expat individual user with Absher
      | key                   | value            |
      | confirmation_code     | 0000             |
      | number                | 000000000        |
      | prefix                | 966              |
      | code                  | 000000           |
      | locale                | en               |
      | login                 | random           |
    Then get notification service request to assert for "Identity created"
    And check expat user retrieves info from Laborer and Working Permit service
    And check expat user was added to company retrieved from Laborer service