@api
Feature: Login access endpoint

  Scenario: Login with valid login and password
    When I post valid login and password via /session end point
    Then I get next response
      | response_key | result |
      | status_code  | 200    |
      | reason       | OK     |
    And proceed two-factor authentication

  Scenario: Change workspace
    When I sign in as Company admin
    And get which companies user has in workspace
    Then session can be patched by another company from user workspace
