import json
import os
import requests

from features.lib.helpers.configurator import Configurator

__config_folder_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', 'config')


class Api(object):
    def __init__(self, user_email=None, password=None):
        self.__config = Configurator()

        self.auth_url = self.__config.auth_api_host
        self.registry_api_url = self.__config.registry_api_host
        self.notifications_api_url = self.__config.notifications_api_host
        self.announcements_api_url = self.__config.announcements_api_host
        self.zoho_exchanger_api_url = self.__config.zoho_exchanger_api_host
        self.trainings_api_url = self.__config.trainings_api_host
        self.logger_api_url = self.__config.logger_api_host

        self.user_email = self.__config.takamol_admin if user_email is None else user_email
        self.password = self.__config.default_password if password is None else password

        self.json_headers = {
            'Content-Type': 'application/json;charset=utf-8'
        }
        self.session = requests.session()

    def login(self, authenticate=True):
        response = self.login_as_user(server=self.auth_url, user_email=self.user_email, password=self.password)
        response_auth = self.proceed_two_factor_authentication() if authenticate else None
        qiwa_token = next((c.value for c in response.cookies if 'qiwa_session' in c.name), None)
        qiwa_sso = next((c.value for c in response_auth.cookies if 'QIWA_SSO' in c.name), None)
        qiwa_signed_in = next((c.value for c in response_auth.cookies if 'QIWA_SIGNED_IN' in c.name), None)
        self.json_headers['qiwa_session'] = qiwa_token
        self.json_headers['QIWA_SSO'] = qiwa_sso
        self.json_headers['QIWA_SIGNED_IN'] = qiwa_signed_in
        return response

    def __get(self, url, endpoint, body=None, params=None):
        return self.session.get(url=url + endpoint, data=body, params=params, headers=self.json_headers)

    def __post(self, url, endpoint, body=None, params=None, headers=None):
        return self.session.post(url=url + endpoint, data=body, params=params,
                                 headers=self.json_headers if not headers else headers)

    def __delete(self, url, endpoint, body=None, params=None):
        return self.session.delete(url=url + endpoint, data=body, params=params, headers=self.json_headers)

    def __patch(self, url, endpoint, body=None, params=None):
        return self.session.patch(url=url + endpoint, data=body, params=params, headers=self.json_headers)

    def __put(self, url, endpoint, body=None, params=None):
        return self.session.put(url=url + endpoint, data=body, params=params, headers=self.json_headers)

    def login_as_user(self, server, user_email, password=None):
        body = {
                "login": user_email,
                "password": password
        }
        return self.__post(url=server, endpoint='/session', body=json.dumps(body))

    def check_absher_code(self, national_id, code):
        body = {
            "user":
                {
                    "national_id": national_id,
                    "code": code
                }
        }
        response = self.__post(url=self.auth_url, endpoint='/users/check_absher_code', body=json.dumps(body))
        return response

    def get_phone_number_code(self, prefix, number, locale):
        body = {
            "phone": {
                "prefix": prefix,
                "number": number,
                "locale": locale
            }
        }
        response = self.__post(url=self.auth_url, endpoint='/phone_number/get_code', body=json.dumps(body))
        return response

    def create_absher_user(self, confirmation_code, number, prefix, user_code, locale, login, national_id,
                           pwd, pwd_confirm):
        headers = self.json_headers.copy()
        headers['content-type'] = 'application/json;charset=utf-8'
        body = {
            "phone":
                {
                    "confirmation_code": confirmation_code,
                    "number": number,
                    "prefix": prefix
                },
            "user":
                {
                    "code": user_code,
                    "locale": locale,
                    "login": login,
                    "national_id": national_id,
                    "password": pwd,
                    "password_confirmation": pwd_confirm
                }
        }
        response = self.__post(url=self.auth_url, endpoint='/users/create/absher', body=json.dumps(body),
                               headers=headers)
        return response

    def proceed_two_factor_authentication(self, auth_code='0000'):
        body = {
            "session": {
                "code": auth_code
            }
        }
        response = self.__post(url=self.auth_url, endpoint='/session/check_by_mobile', body=json.dumps(body))
        return response

    def get_current_session(self):
        response = self.__get(url=self.auth_url, endpoint='/session')
        return response

    def get_users_list(self, page_number=1, per_page=25):
        criteria = '?page={}&per_page={}'.format(page_number, per_page)
        response = self.__get(url=self.auth_url, endpoint='/users%s' % criteria)
        return response

    def find_user_by_national_id(self, national_id):
        response = json.loads(self.get_users_list().text)
        total_pages = response['meta']['total_pages']
        for i in xrange(total_pages):
            users_list = json.loads(self.get_users_list(page_number=i, per_page=50).text)['items']
            for user in users_list:
                if user['national_id'] == national_id:
                    return user

    def find_user_by_email(self, user_email):
        response = json.loads(self.get_users_list().text)
        total_pages = response['meta']['total_pages']
        for i in xrange(total_pages):
            users_list = json.loads(self.get_users_list(page_number=i, per_page=50).text)['items']
            for user in users_list:
                if user['notification_email'] == user_email:
                    return user

    def get_notification_service_list(self):
        response = self.__get(url=self.notifications_api_url, endpoint='/notifications')
        return response

    def get_notification_by_user_id(self, user_id):
        response = self.__get(url=self.notifications_api_url, endpoint='/notifications/%s' % user_id)
        return response

    def get_user_info(self, user_id):
        response = self.__get(url=self.auth_url, endpoint='/users/%s' % user_id)
        return response

    def delete_user(self, user_id):
        response = self.__delete(url=self.auth_url, endpoint='/admin/users/%s' % user_id)
        return response

    def change_workspace(self, company_id):
        body = {
            "session": {
                "company_id": company_id
            }
        }
        response = self.__patch(url=self.auth_url, endpoint='/session', body=json.dumps(body))
        return response