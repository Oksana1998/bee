import json
import os
import yaml

__config_folder_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'config')

config_path = os.path.join(__config_folder_path, 'config.local.yml' if 'config.local.yml' in os.listdir(
    __config_folder_path) else 'config.yml')
env_config_path = os.path.join(__config_folder_path, 'env_config.json')


def get_config():
    with open(os.path.join(config_path), 'r') as ymlfile:
        return yaml.load(ymlfile)


def get_env_config():
    with open(os.path.join(env_config_path), 'r') as f:
        return json.load(f)


class Configurator(object):
    __config = get_config()
    env_config = get_env_config()

    PAGE_URL_BY_NAME = {
        'Login': '{}/en{}'.format(__config.get('auth_url'), __config.get('login_url')),
        'Company expats': '{}/en{}'.format(__config.get('base_url'), __config.get('company_expats_url')),
        'Sign Up': '{}/en{}'.format(__config.get('auth_url'), __config.get('sign_up_url'))
    }

    USER_ROLES = {
        'Takamol admin': __config.get('takamol_admin'),
        'Company admin': __config.get('company_admin'),
        'Company Iqama admin': __config.get('company_admin_iqama'),
        'Company user': __config.get('company_user'),
        'Product admin': __config.get('product_admin'),
        'Employee': __config.get('employee'),
        'invalid user': __config.get('invalid_user')
    }

    USER_PASSWORDS = {
        'Takamol admin password': __config.get('default_password'),
        'Company admin password': __config.get('default_password'),
        'Company user password': __config.get('default_password'),
        'Product admin password': __config.get('default_password'),
        'Employee password': __config.get('default_password'),
        'invalid user password': __config.get('invalid_password'),
    }

    def __init__(self):
        super(Configurator, self)
        self.local_grid = self.__config.get('local_grid')
        self.default_browser = self.__config.get('default_browser')
        self.platform = self.__config.get('platform')

        self.auth_url = self.__config.get('auth_url')
        self.base_url = self.__config.get('base_url')
        self.language_url = self.__config.get('language_url')
        self.env_type = self.__config.get('env_type')

        self.login_url = self.__config.get('login_url')
        self.sign_up_url = self.__config.get('sign_up_url')
        self.workspaces_url = self.__config.get('workspaces_url')
        self.company_expats_url = self.__config.get('company_expats_url')
        self.control_panel_url = self.__config.get('control_panel_url')
        self.dashboard_url = self.__config.get('dashboard_url')
        self.e_services_url = self.__config.get('e_services_url')

        self.takamol_admin = self.__config.get('takamol_admin')
        self.company_admin = self.__config.get('company_admin')
        self.product_admin = self.__config.get('product_admin')
        self.company_user = self.__config.get('company_user')
        self.employee = self.__config.get('employee')
        self.invalid_user = self.__config.get('invalid_user')

        self.takamol_admin_password = self.__config.get('default_password')
        self.company_admin_password = self.__config.get('default_password')
        self.company_user_password = self.__config.get('default_password')
        self.product_admin_password = self.__config.get('default_password')
        self.employee_password = self.__config.get('default_password')
        self.default_password = self.__config.get('default_password')
        self.invalid_password = self.__config.get('invalid_password')

        self.auth_api_host = next((c['value'] for c in self.env_config[self.env_type]
                                   if 'auth-api-host' in c['key']), None)
        self.registry_api_host = next((c['value'] for c in self.env_config[self.env_type]
                                       if 'registry-api-host' in c['key']), None)
        self.notifications_api_host = next((c['value'] for c in self.env_config[self.env_type]
                                            if 'notifications-api-host' in c['key']), None)
        self.announcements_api_host = next((c['value'] for c in self.env_config[self.env_type]
                                            if 'announcements-api-host' in c['key']), None)
        self.zoho_exchanger_api_host = next((c['value'] for c in self.env_config[self.env_type]
                                             if 'zoho-exchanger-api-host' in c['key']), None)
        self.trainings_api_host = next((c['value'] for c in self.env_config[self.env_type]
                                        if 'trainings-api-host' in c['key']), None)
        self.logger_api_host = next((c['value'] for c in self.env_config[self.env_type]
                                     if 'logger-api-host' in c['key']), None)

    # Defines pag url from config according to requested page page name
    def define_page_url(self, requested_page):
        return self.select_page_url(requested_page)

    def select_page_url(self, requested_url):
        try:
            return self.PAGE_URL_BY_NAME[requested_url]
        except KeyError:
            return 'No such page url exists in config'

    # Defines email from from config according to requested user role
    def define_user_role(self, requested_role):
        return self.select_user_role(requested_role)

    def select_user_role(self, requested_role):
        try:
            return self.USER_ROLES[requested_role]
        except KeyError:
            return 'No such user role exists in config'

    # Defines user's password from config according to requested user
    def define_user_password(self, requested_user_password):
        return self.select_user_password(requested_user_password)

    def select_user_password(self, requested_user_password):
        try:
            return self.USER_PASSWORDS[requested_user_password]
        except KeyError:
            return 'No such user password exists in config'

    def define_two_fa_code(self, two_fa_code):
        return two_fa_code
