from random import randint
import fauxfactory


def id_generator(size=8, utf8=False):
    if utf8:
        alphasize = randint(1, size-1)
        utfsize = size - alphasize
        result = fauxfactory.gen_alphanumeric(alphasize) + fauxfactory.gen_utf8(utfsize)
    else:
        result = fauxfactory.gen_alphanumeric(size)
    return result


def email_generator(name=None, domain="qiwa", tlds="info", prefix='user'):
    if not name:
        name = id_generator(utf8=False)
    email = "%s_%s" % (prefix, fauxfactory.gen_email(name=name, domain=domain, tlds=tlds))
    return email.lower()


def random_national_id(saudi=True):
    if saudi:
        return '{}{}'.format(1, randint(111111111, 900000000))
    else:
        return '{}{}'.format(2, randint(111111111, 900000000))
