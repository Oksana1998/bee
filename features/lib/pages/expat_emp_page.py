from selenium.webdriver.common.by import By
from features.lib.pages import BasePage


class ExpatsPage(BasePage):
    locator_dictionary = {
        'expat_statistics_table': [By.XPATH,'//div[@class="q-page-box__header"]//*[text()="Expats statistics"]'],
        'expats_management_table': [By.XPATH, '//div[contains(@class,"expats-box")]'],
        'issue_wp_button': [By.XPATH, '//div[@class="content__btn-holder col-12 right"]'
                                      '//a[@href="/company/expats/renew-working-permit"]'],
        'records_results': [By.XPATH, '//span[text()="results"]/ancestor::div[@class="level-left"]'],
        'pagination_block': [By.XPATH, '//div[@class="pagination"]']
    }

    def __init__(self, driver):
        super(ExpatsPage, self).__init__(driver)
        self.base_url = '{}{}{}'.format(
            self.config_env.base_url, self.config_env.language_url, self.config_env.company_expats_url)
