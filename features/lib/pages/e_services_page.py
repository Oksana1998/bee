from selenium.webdriver.common.by import By
from features.lib.pages import BasePage


class EServicesPage(BasePage):
    locator_dictionary = {
        'saudization_certificate': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Saudization Certificate"]'],
        'change_occupation': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Change Occupation"]'],
        'transferring_worker': [By.XPATH,  '//div[@class="q-page-box__header"]//*[text()="Transferring Worker"]'],
        'working_permit': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Issue & Renew Working Permits"]'],
        'visa_issuance_service': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Visa Issuance service"]'],
        'labor_market_index': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Labor Market Index"]'],
        'labor_policies': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Labor Policies"]'],
        'national_address_registration': [By.XPATH, '//div[@class="q-page-box__header"]'
                                                    '//*[text()="National Address Registration"]']
    }

    def __init__(self, driver):
        super(EServicesPage, self).__init__(driver)
        self.base_url = '{}{}{}'.format(
            self.config_env.base_url, self.config_env.language_url, self.config_env.e_services_url
        )
