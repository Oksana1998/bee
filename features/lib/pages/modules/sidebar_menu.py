from selenium.webdriver.common.by import By
from features.lib.helpers.configurator import Configurator
from features.lib.pages import BasePage


class SideBarMenus(BasePage):
    locator_dictionary = {
        # Top sidebar menu:
        'dropdown_user_info': (By.XPATH, '//div[contains(@class,"dropdown user__info")]'),

        # Left Sidebar menu:
        'dashboard_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Dashboard"]'),
        'expat_employees_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Expat employees"]'),
        'delegation_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Delegation"]'),
        'control_panel_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Control Panel"]'),
        'subscriptions_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Subscriptions"]'),
        'companies_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Companies"]'),
        'users_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Users"]'),
        'invoices_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Invoices"]'),
        'transactions_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Transactions"]'),
        'announcements_menu': (By.XPATH, '//nav[@class="navigation"]//a//span[text()="Announcements"]'),
    }

    MENU_ELEMENT_BY_MENU_NAME = {
        'Dashboard': 'dashboard_menu',
        'Expat employees': 'expat_employees_menu',
        'Delegation': 'delegation_menu',
        'Control Panel': 'control_panel_menu',
        'Subscriptions': 'subscriptions_menu',
        'Companies': 'companies_menu',
        'Users': 'users_menu',
        'Invoices': 'invoices_menu',
        'Transactions': 'transactions_menu',
        'Announcements': 'announcements_menu'
    }

    def __init__(self, driver):
        super(SideBarMenus, self).__init__(driver)
        self.config = Configurator()

    def define_menu_item(self, requested_menu):
        return self.select_menu_item(requested_menu)

    def select_menu_item(self, requested_menu):
        try:
            return self.MENU_ELEMENT_BY_MENU_NAME[requested_menu]
        except KeyError:
            return 'No such "%s" menu exists in config' % requested_menu

    def open_user_menu(self):
        self.dropdown_user_info.click()
        assert self.is_visible(self.dropdown_user_info), 'User dropdown menu didn\'t become visible'

    def open_left_menu_item(self, menu_name):
        exec('self.%s.click()' % menu_name)
