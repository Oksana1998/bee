from selenium.webdriver.common.by import By
from features.lib.pages import BasePage


class DashboardPage(BasePage):
    locator_dictionary = {
        'company_profile_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Company profile"]'],
        'commercial_record_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Commercial record"]'],
        'labor_index_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Labor Index"]'],
        'expat_employees_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Expat employees"]'],
        'saudization_certificate': [By.XPATH, '//div[@class="q-page-box__header"]'
                                              '//*[text()="Saudization Certificate"]'],
        'mowaamah_certificate': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Mowaamah Certification"]'],
        'recent_activities_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Recent activities"]'],
        'reminders_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Reminder"]'],
        'labor_policies_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Labor Policies"]']
    }

    def __init__(self, driver):
        super(DashboardPage, self).__init__(driver)
        self.base_url = '{}{}{}'.format(
            self.config_env.base_url, self.config_env.language_url, self.config_env.dashboard_url
        )
