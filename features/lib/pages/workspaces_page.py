from selenium.webdriver.common.by import By
from features.lib.pages import BasePage


class WorkspacesPage(BasePage):
    locator_dictionary = {
        'select_account_header': [By.XPATH, '//*[text()="Choose an account"]'],

    }

    def __init__(self, driver):
        super(WorkspacesPage, self).__init__(driver)
        self.base_url = '{}{}'.format(self.config_env.auth_url, self.config_env.workspaces_url)
