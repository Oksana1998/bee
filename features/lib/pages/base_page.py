import pickle
import traceback
from time import sleep

from features.lib.helpers.configurator import Configurator
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC, ui
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, NoSuchElementException


class BasePage(object):

    config_env = Configurator()

    def __init__(self, driver):
        super(BasePage, self).__init__()
        self.base_url = self.config_env.base_url
        self.driver = driver
        self.timeout = 15

    def find_element(self, selector, locator):
        ui.WebDriverWait(self.driver, self.timeout).until(
            EC.presence_of_element_located((selector, locator)))
        return self.driver.find_element(by=selector, value=locator)

    def find_elements(self, selector, locator):
        ui.WebDriverWait(self.driver, self.timeout).until(
            EC.visibility_of_all_elements_located((selector, locator)))
        return self.driver.find_elements(by=selector, value=locator)

    def visit(self, url):
        self.driver.get(url)

    def hover(self, element):
        ActionChains(self.driver).move_to_element(element).perform()

    def __getattr__(self, what):
        try:
            if what in self.locator_dictionary.keys():
                try:
                    WebDriverWait(self.driver, self.timeout).until(
                        EC.presence_of_element_located(self.locator_dictionary[what]),
                        message='Element "%s" is not presented in DOM model' % what
                    )
                except(TimeoutException, StaleElementReferenceException):
                    traceback.print_exc()

                try:
                    WebDriverWait(self.driver, self.timeout).until(
                        EC.visibility_of_element_located(self.locator_dictionary[what]),
                        message='Element "%s" did not become visible' % what
                    )
                except(TimeoutException, StaleElementReferenceException):
                    traceback.print_exc()

                try:
                    WebDriverWait(self.driver, self.timeout).until(
                        EC.element_to_be_clickable(self.locator_dictionary[what]),
                        message='Element "%s" did not become clickable' % what
                    )
                except(TimeoutException, StaleElementReferenceException):
                    traceback.print_exc()

                return self.find_element(*self.locator_dictionary[what])
        except AttributeError:
            super(BasePage, self).__getattribute__("method_missing")(what)

    def method_missing(self, what):
        print 'No such "%s" element described in %s' % (what, self.__class__.__name__)

    def check_element_exists(self, what=None, selector=By.XPATH, locator=None):
        try:
            if not what:
                self.find_element(selector, locator)
            else:
                self.find_element(*what)
        except NoSuchElementException:
            return False
        return True

    # returns True if element is visible within 13 seconds, otherwise False
    def is_visible(self, what=None, select_by=None, locator=None, timeout=13):
        try:
            if what:
                ui.WebDriverWait(self.driver, timeout).until(
                    EC.visibility_of_element_located(self.locator_dictionary[what]))
                return True
            elif not what and select_by is None:
                return False
            else:
                ui.WebDriverWait(self.driver, timeout).until(
                    EC.visibility_of_element_located((select_by, locator)))
                return True
        except KeyError:
            raise Exception('No "%s" element described in page object!' % what)
        except TimeoutException:
            return False

    # returns True if element is NOT visible within 13 seconds, otherwise False
    def is_not_visible(self, what=None, select_by=None, locator=None, timeout=13):
        try:
            if what:
                ui.WebDriverWait(self.driver, timeout).until_not(
                    EC.visibility_of_element_located(self.locator_dictionary[what]))
                return True
            elif not what and select_by is None:
                return False
            else:
                ui.WebDriverWait(self.driver, timeout).until_not(
                    EC.visibility_of_element_located((select_by, locator)))
                return True
        except KeyError:
            raise Exception('No "%s" element described in page object!' % what)
        except TimeoutException:
            return False

    def refresh_page(self):
        self.driver.refresh()
        sleep(1)

    def scroll_to_element(self, element):
        self.driver.execute_script("arguments[0].scrollIntoView();", element)

    def reset_cookies(self):
        self.driver.delete_all_cookies()

    def save_cookie(self, path):
        with open(path, 'wb') as filehandler:
            pickle.dump(self.driver.get_cookies(), filehandler)

    def load_cookie(self, path):
        with open(path, 'rb') as cookiesfile:
            cookies = pickle.load(cookiesfile)
            for cookie in cookies:
                self.driver.add_cookie(cookie)
