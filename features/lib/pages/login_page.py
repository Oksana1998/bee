from selenium.webdriver.common.by import By
from features.lib.pages import BasePage


class LoginPage(BasePage):
    locator_dictionary = {
        'login_field': [By.ID, 'login_username'],
        'password_field': [By.XPATH, '//input[@name="login_password"]'],
        'remember_me_checkbox': [By.XPATH, '//input[@id="rememberMe"]/ancestor::div[@class="checkbox"]'],
        'continue_button': [By.ID, 'continue_button'],
        'two_factor_authentication_field': [By.ID, 'code'],
        'sign_in_button': [By.ID, 'sign_in_button'],
        'successful_message': [By.CSS_SELECTOR, 'p.w-text--big']
    }

    def __init__(self, driver):
        super(LoginPage, self).__init__(driver)
        self.base_url = '{}{}{}'.format(
            self.config_env.auth_url, self.config_env.language_url, self.config_env.login_url)

    def specify_login(self, email):
        self.login_field.clear()
        self.login_field.send_keys(email)

    def specify_login_password(self, pwd):
        self.password_field.clear()
        self.password_field.send_keys(pwd)

    def tick_remember_me(self, tick=True):
        self.remember_me_checkbox.click() if tick else None

    def click_continue(self):
        self.continue_button.click()

    def specify_2fa_code(self, code='0000'):
        self.two_factor_authentication_field.clear()
        self.two_factor_authentication_field.send_keys(code)

    def click_sign_in_button(self):
        self.sign_in_button.click()
