from selenium.webdriver.common.by import By
from features.lib.pages import BasePage


class SignUpPage(BasePage):
    locator_dictionary = {
        'national_id_field': [By.ID, 'absherId'],
        'birthday_day': [By.ID, 'birthdayDay'],
        'birthday_month': [By.ID, 'birthdayMonth'],
        'birthday_year': [By.ID, 'birthdayYear'],
        'verify_info_button': [By.ID, 'verifyAbsher'],
        'registration_step_2': [By.ID, 'page-switcher-2'],
        'absher_code_field': [By.ID, 'absherCode'],
        'next_button_1': [By.ID, 'nextStepAbsher2'],
        'email_field': [By.ID, 'email'],
        'password_field': [By.ID, 'password'],
        'password_confirmation_field': [By.ID, 'passwordConfirmation'],
        'next_button_2': [By.ID, 'nextStep3'],
        'perfix_number_field': [By.ID, 'phonePrefix'],
        'phone_number_field': [By.ID, 'phoneNumber'],
        'confirmation_code_field': [By.ID, 'confirmationCode'],
        'terms_agree_checkbox': [By.XPATH, '//span[text()="I agree with"]'],
        'create_account_button': [By.ID, 'createAccount'],
        'success_registration': [By.XPATH, '//div[@class="sign-template success-registration"]'],
        'validation_error_message': [By.XPATH, '//div[@class="validation-message validation-message--margin-bottom"]'
                                               '//*[text()="Your national ID or birthdate is incorrect.'
                                               ' Please try again"]'],
        'internal_service_error_message': [By.XPATH, '//div[@class="validation-message validation-message--margin-bottom"]'
                                         '//*[text()="Something went wrong. Please, try again."]']
    }

    def __init__(self, driver):
        super(SignUpPage, self).__init__(driver)
        self.base_url = '{}/en{}'.format(self.config_env.auth_url, self.config_env.sign_up_url)

    def specify_national_id(self, national_id):
        self.national_id_field.clear()
        self.national_id_field.send_keys(national_id)

    @staticmethod
    def select_birthday_day(element, value):
        for option in element.find_elements_by_tag_name('option'):
            if option.text == value:
                option.click()
                break

    @staticmethod
    def select_birthday_month(element, value):
        for option in element.find_elements_by_tag_name('option'):
            if option.text == value:
                option.click()
                break

    @staticmethod
    def select_birthday_year(element, value):
        for option in element.find_elements_by_tag_name('option'):
            if option.text == value:
                option.click()
                break

    def click_on_verify_info_button(self):
        self.verify_info_button.click()

    def enter_absher_confirmation_code(self, absher_code):
        self.absher_code_field.clear()
        self.absher_code_field.send_keys(absher_code)

    def click_on_next_button_1(self):
        self.next_button_1.click()

    def specify_email(self, email):
        self.email_field.clear()
        self.email_field.send_keys(email)

    def specify_password(self, pwd):
        self.password_field.clear()
        self.password_field.send_keys(pwd)

    def confirm_password(self, confirm_pwd):
        self.password_confirmation_field.clear()
        self.password_confirmation_field.send_keys(confirm_pwd)

    def click_on_next_button_2(self):
        self.next_button_2.click()

    def specify_perfix_number(self, perfix_number):
        self.perfix_number_field.clear()
        self.perfix_number_field.send_keys(perfix_number)

    def specify_phone_number(self, number):
        self.phone_number_field.clear()
        self.phone_number_field.send_keys(number)

    def enter_qiwa_confirmation_code(self, qiwa_code):
        self.confirmation_code_field.clear()
        self.confirmation_code_field.send_keys(qiwa_code)

    def tick_terms_agree_checkbox(self):
        self.terms_agree_checkbox.click()

    def click_on_create_account_button(self):
        self.create_account_button.click()
