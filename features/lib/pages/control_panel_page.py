from selenium.webdriver.common.by import By
from features.lib.pages import BasePage


class ControlPanelPage(BasePage):
    locator_dictionary = {
        'roles_management_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Roles Management"]'],
        'product_usage_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Product usage"]'],
        'subscriptions_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Subscriptions"]'],
        'invoices_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Invoices"]'],
        'support_tickets_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Support tickets"]'],
        'switch_accounts_card': [By.XPATH, '//div[@class="q-page-box__header"]//*[text()="Switch accounts"]']
    }

    def __init__(self, driver):
        super(ControlPanelPage, self).__init__(driver)
        self.base_url = '{}{}{}'.format(
            self.config_env.base_url, self.config_env.language_url, self.config_env.control_panel_url)
