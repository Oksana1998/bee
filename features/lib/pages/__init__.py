from .base_page import BasePage
from .login_page import LoginPage
from .expat_emp_page import ExpatsPage
from .workspaces_page import WorkspacesPage
from .control_panel_page import ControlPanelPage
from .sign_up_page import SignUpPage
from .dashboard_page import DashboardPage
from .e_services_page import EServicesPage
