from time import sleep
from requests import ConnectionError
from selenium import webdriver
from selenium.common.exceptions import InvalidArgumentException
from selenium.webdriver import DesiredCapabilities
from features.lib.helpers.api_client.api_client import Api
from features.lib.helpers.rp_driver import (start_launcher, start_feature_test, timestamp, start_scenario_test,
                                            log_step_result, finish_launcher, terminate_service,
                                            finish_scenario_test, finish_feature)
from features.lib.pages import BasePage
from behave.model import Scenario


def before_all(context):
    # Continues after failed step in scenario
    Scenario.continue_after_failed_step = context.config.userdata.getbool('runner.continue_after_failed_step', False)

    context.browser_name = context.config.userdata.get('BROWSER', BasePage.config_env.default_browser)
    context.grid = str(context.config.userdata.get('GRID', BasePage.config_env.local_grid))
    context.platform = context.config.userdata.get('PLATFORM', BasePage.config_env.platform)
    context.launch_name = context.config.userdata.get('LAUNCH_NAME', 'QIWA Tests').replace('_', ' ')

    launcher_description = 'Server url: %s\nServer type: %s' % (BasePage.config_env.base_url,
                                                                BasePage.config_env.env_type.upper())
    tags_list = []
    for i in context.config.tags.ands[0]:
        tags_list.append(i)
    start_launcher(name='%s for %s' % (
        context.launch_name, 'API' if 'api' in str(tags_list) else context.browser_name.capitalize()),
                   start_time=timestamp(), tags=tags_list, description=launcher_description)

    context.user_ids = []
    context.user_nids = []

    api_client = Api()
    for i in xrange(10):
        try:
            api_client.login()
            api_client.proceed_two_factor_authentication()
            context.api_admin_client = api_client
            break
        except ConnectionError:
            sleep(1)


def before_feature(context, feature):
    context.config.setup_logging()
    # Starts feature
    start_feature_test(name='Feature: %s' % feature.name,
                       description='Feature description:\n%s' % '\n'.join(feature.description),
                       tags=feature.tags,
                       start_time=timestamp(),
                       item_type='SUITE')

    browser_capability = {
        'chrome': DesiredCapabilities.CHROME.copy(),
        'firefox': DesiredCapabilities.FIREFOX.copy(),
        'edge': DesiredCapabilities.EDGE.copy(),
        'ie': DesiredCapabilities.INTERNETEXPLORER.copy(),
        'safari': DesiredCapabilities.SAFARI.copy()
    }
    capabilities = browser_capability[context.browser_name]
    capabilities['javascriptEnabled'] = True
    capabilities['acceptSslCerts'] = True
    capabilities['elementScrollBehavior'] = 1 if context.browser_name == 'firefox' else None
    capabilities['platform'] = context.platform
    capabilities['safari.options'] = {
        'technologyPreview': True
    } if context.browser_name == 'safari' else None
    context.driver = webdriver.Remote(command_executor=context.grid, desired_capabilities=capabilities)
    try:
        context.driver.set_page_load_timeout(30)
    except InvalidArgumentException:
        pass
    context.driver.maximize_window()
    if 'Certificate error' in context.driver.title:
        context.driver.get("javascript:document.getElementById('invalidcert_continue').click()")


def before_scenario(context, scenario):
    # Start test item.
    start_scenario_test(name='Scenario: %s' % scenario.name, description='Test for %s' % context.browser_name,
                        tags=scenario.tags, start_time=timestamp(), item_type='SCENARIO')


def after_step(context, step):
    # Create text log message with INFO level.
    if step.table:
        table_data = []
        for row in step.table.rows:
            table_data.append('|'.join(row))
        result = "|%s|" % '|\n|'.join(table_data)
        log_step_result(end_time=timestamp(),
                        message='%s %s\n~~~~~~~~~~~~~~~~~~~~~~~~~\nTable data:\n%s' % (step.keyword, step.name, result),
                        level='INFO')
    else:
        log_step_result(end_time=timestamp(), message='%s %s' % (step.keyword, step.name), level='DEBUG')

    if step.status == 'failed':
        # Creates log message with attachment and ERROR level.
        if 'api' in context.tags:
            log_step_result(end_time=timestamp(), message=step.exception.message, level='ERROR')
        else:
            log_step_result(end_time=timestamp(), message=step.exception.message, level='ERROR',
                            attachment={
                                'name': 'failed_scenario_name',
                                'data': context.driver.get_screenshot_as_png(),
                                'mime': 'application/octet-stream'
                            })


def after_scenario(context, scenario):
    for step in scenario.steps:
        if step.status.name == 'undefined':
            log_step_result(end_time=timestamp(),
                            message='%s %s - step is undefined.\nPlease define step.' % (step.keyword, step.name),
                            level='WARN')
        elif step.status.name == 'skipped':
            log_step_result(end_time=timestamp(),
                            message='%s %s - %s' % (step.keyword, step.name, step.status.name.capitalize()),
                            level='TRACE')

    if scenario.status == 'failed':
        finish_scenario_test(end_time=timestamp(), status='FAILED')
    else:
        finish_scenario_test(end_time=timestamp(), status='PASSED')


def after_feature(context, feature):
    context.driver.quit() if 'api' not in feature.tags else None
    for i in feature.scenarios:
        try:
            if i.tags[0] == 'skip':
                start_scenario_test(name='Scenario: %s' % i.name,
                                    description='Skipped due to `%s` issue' % i.description[0].split('#')[1],
                                    tags=feature.tags, start_time=timestamp(), item_type='SCENARIO')
                finish_scenario_test(end_time=timestamp(), status='SKIPPED')
        except IndexError:
            pass
    finish_feature(end_time=timestamp(), status='FAILED') if feature.status == 'failed' else finish_feature(
        end_time=timestamp(), status='PASSED')


def after_all(context):
    # Finishes launcher.
    finish_launcher(end_time=timestamp())
    terminate_service()

    for i in context.user_ids:
        context.api_admin_client.delete_user(i)

    for i in context.user_nids:
        search_result = context.api_admin_client.find_user_by_national_id(i)
        context.api_admin_client.delete_user(search_result['id'])
