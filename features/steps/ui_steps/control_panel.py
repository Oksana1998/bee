from behave import use_step_matcher, when, then, step
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from features.lib.pages import ControlPanelPage

use_step_matcher("re")


@when('verify "Control panel" page has the next cards')
def control_panel_cards(context):
    page = ControlPanelPage(context.driver)
    assert page.is_visible('roles_management_card'), '"Roles Management" card is not visible'
    assert page.is_visible('product_usage_card'), '"Roles Management" card is not visible'
    assert page.is_visible('subscriptions_card'), '"Roles Management" card is not visible'
    assert page.is_visible('invoices_card'), '"Roles Management" card is not visible'
    assert page.is_visible('support_tickets_card'), '"Roles Management" card is not visible'
    assert page.is_visible('switch_accounts_card'), '"Roles Management" card is not visible'


@then('"Roles management" card contains the next columns order')
def roles_management_card_columns(context):
    columns_list = []
    for i in context.table:
        columns_list.append(i[0])

    page = ControlPanelPage(context.driver)
    table_heads = page.find_elements(By.XPATH,
                                     '//div[contains(@class,"roles-box")]//thead//tr//span[@class="label"]')
    table_columns = []
    for i in table_heads:
        table_columns.append(i.text)

    assert columns_list == table_columns, '{}{}{}'.format(
        'Order or presence of columns is not correct\n',
        "Expected:\nColumns amount: %s\nOrder: %s" % (len(columns_list), columns_list),
        "Got:\nColumns amount: %s\nOrder: %s" % (len(table_columns), table_columns)
    )


@step('"Invoices" card contains the next columns')
def invoices_card_columns(context):
    columns_list = []
    for i in context.table:
        columns_list.append(i[0])

    page = ControlPanelPage(context.driver)
    table_heads = []
    try:
        table_heads = page.find_elements(By.XPATH,
                                         '//div[contains(@class,"invoices-box")]//thead//tr//span[@class="label"]')
    except TimeoutException:
        pass

    if len(table_heads) > 0:
        table_columns = []
        for i in table_heads:
            table_columns.append(i.text)

        assert columns_list == table_columns, '{}{}{}'.format(
            'Order or presence of columns is not correct\n',
            "Expected:\nColumns amount: %s\nOrder: %s" % (len(columns_list), columns_list),
            "Got:\nColumns amount: %s\nOrder: %s" % (len(table_columns), table_columns)
        )
