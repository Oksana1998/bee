from behave import use_step_matcher, then, step
from selenium.webdriver.common.by import By
from features.lib.pages import ExpatsPage

use_step_matcher("re")


@then('verify page has the next elements')
def verify_page_has_the_next_elements(context):
    page = ExpatsPage(context.driver)
    assert page.is_visible('expat_statistics_table'), '"Expats statistics" table is not visible or absent'
    assert page.is_visible('expats_management_table'), '"Expats management" table is not visible or absent'
    assert page.is_visible('issue_wp_button'), 'Issue or renew WP button is not visible'
    results = int(page.records_results.text.split(' ')[0])
    if results > 10:
        assert page.is_visible('pagination_block'), 'Pagination is not visible or absent'


@step('Expat management table contains columns in next order')
def expat_management_table_columns_order(context):
    columns_list = []
    for i in context.table:
        columns_list.append(i[0])

    page = ExpatsPage(context.driver)
    table_heads = page.find_elements(By.XPATH,
                                     '//div[contains(@class,"expats-box")]//thead//tr//span[@class="label"]')
    table_columns = []
    for i in table_heads:
        table_columns.append(i.text)

    assert columns_list == table_columns, '{}{}{}'.format(
        'Order or presence of columns is not correct\n',
        "Expected:\nColumns amount: %s\nOrder: %s" % (len(columns_list), columns_list),
        "Got:\nColumns amount: %s\nOrder: %s" % (len(table_columns), table_columns)
    )
