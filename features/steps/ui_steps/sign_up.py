from behave import use_step_matcher, when, step, then
from selenium.webdriver.common.by import By

from features.lib.pages.sign_up_page import SignUpPage

use_step_matcher("re")


@when("I specify National id number (?P<national_id>.+)")
def specify_national_id(context, national_id):
    context.user_nids.append(national_id)
    page = SignUpPage(context.driver)
    page.specify_national_id(national_id)


@when("I specify Iqama id number (?P<iqama_id>.+)")
def specify_iqama_id(context, iqama_id):
    context.user_nids.append(iqama_id)
    page = SignUpPage(context.driver)
    page.specify_national_id(iqama_id)


@step("enter birthday in Hijiri")
def enter_birthday(context):
    birthday_table = context.table.rows
    users_birthday = {}
    for item in birthday_table:
        users_birthday[item[0]] = item[1]
    assert users_birthday
    page = SignUpPage(context.driver)
    page.select_birthday_day(page.birthday_day, users_birthday['day'])
    page.select_birthday_month(page.birthday_month, users_birthday['month'])
    page.select_birthday_year(page.birthday_year, users_birthday['year'])

@step('click on "Verify my information" button')
def click_on_verify_info_button(context):
    page = SignUpPage(context.driver)
    page.click_on_verify_info_button()

@step("check that birthday fields are disabled")
def check_disabled_birthday_fields(context):
    page = SignUpPage(context.driver)
    day = page.find_element(selector=By.ID, locator='birthdayDay')
    month = page.find_element(selector=By.ID, locator='birthdayMonth')
    year = page.find_element(selector=By.ID, locator='birthdayYear')
    assert day.get_attribute('disabled'), '"Day" disabled attribute was not found'
    assert month.get_attribute('disabled'), '"Month" disabled attribute was not found'
    assert year.get_attribute('disabled'), '"Year" disabled attribute was not found'


@step("switch to the second step")
def switch_to_second_step(context):
    page = SignUpPage(context.driver)
    assert page.is_visible('registration_step_2'), 'Second step is not found'
    switcher = page.locator_dictionary['registration_step_2']
    switcher_element = page.find_element(switcher[0], switcher[1])
    context.driver.execute_script("arguments[0].removeAttribute('disabled')", switcher_element)
    page.registration_step_2.click()


@step("enter Absher confirmation code (?P<absher_code>.+)")
def enter_absher_code(context, absher_code):
    page = SignUpPage(context.driver)
    page.enter_absher_confirmation_code(absher_code)


@step('click on "Next" button in the second step')
def click_on_next_button_in_step_2(context):
    SignUpPage(context.driver).click_on_next_button_1()


@step("enter valid email as (?P<email>.+)")
def specify_valid_email(context, email):
    SignUpPage(context.driver).specify_email(email)


@step("enter valid password as (?P<pwd>.+)")
def enter_valid_password(context, pwd):
    SignUpPage(context.driver).specify_password(pwd)


@step("confirm password with (?P<pwd_confirmation>.+)")
def confirm_password(context, pwd_confirmation):
    SignUpPage(context.driver).confirm_password(pwd_confirmation)


@step('click on "Next" button in the third step')
def click_on_next_button_in_step_3(context):
    SignUpPage(context.driver).click_on_next_button_2()


@step("enter phone number prefix (?P<perfix_number>.+)")
def enter_perfix_number(context, perfix_number):
    SignUpPage(context.driver).specify_perfix_number(perfix_number)


@step("enter phone number (?P<number>.+)")
def enter_phone_number(context, number):
    SignUpPage(context.driver).specify_phone_number(number)


@step("enter Qiwa confirmation code (?P<confirmation_code>.+)")
def enter_qiwa_confirmation_code(context, confirmation_code):
    SignUpPage(context.driver).enter_qiwa_confirmation_code(confirmation_code)


@step('tick agree with "Terms and Conditions" checkbox')
def tick_terms_and_conditions_checkbox(context):
    SignUpPage(context.driver).tick_terms_agree_checkbox()


@step('click on "Create account" button')
def click_on_create_account_button(context):
    SignUpPage(context.driver).click_on_create_account_button()


@step('user is registered and can see "(?P<text_message>.+)" message')
def user_see_successful_message(context, text_message):
    page = SignUpPage(context.driver)
    assert page.is_visible(select_by=By.XPATH, locator='//*[contains(text(),"%s")]' % text_message), (
            'locator is: //*[contains(text(),"%s")]' % text_message)


@step('user can see "success registration" page')
def user_see_success_registration_page(context):
    page = SignUpPage(context.driver)
    assert page.is_visible('success_registration'), 'Success regitration page is invisible'


@then('I should see "(?P<text_message>.+)" message')
def user_see_validation_error_message(context, text_message):
    page = SignUpPage(context.driver)
    assert page.is_visible('validation_error_message'), 'Validation error message is invisible'


@step('I shouldn\'t see "(?P<text_message>.+)" message')
def user_cant_see_internal_service_error(context, text_message):
    page = SignUpPage(context.driver)
    assert page.is_not_visible('internal_service_error_message'), "500 internal service error"
