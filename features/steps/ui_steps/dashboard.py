from behave import use_step_matcher, when, then, step
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from features.lib.pages import DashboardPage

use_step_matcher("re")


@then("Verify that page has the next elements")
def verify_dashboard_page_element(context):
    page = DashboardPage(context.driver)
    assert page.is_visible('company_profile_card'), 'Company profile card was not found'
    assert page.is_visible('commercial_record_card'), 'Commercial record card was not found'
    assert page.is_visible('labor_index_card'), 'Labor index" card was not found'
    assert page.is_visible('expat_employees_card'), 'Expat employees card was not found'
    assert page.is_visible('saudization_certificate'), 'Saudization certificate card was not found'
    assert page.is_visible('mowaamah_certificate'), 'Mowaamah certificate card was not found'
    assert page.is_visible('recent_activities_card'), 'Recent activities card was not found'
    assert page.is_visible('reminders_card'), 'Reminders card is not found'
    assert page.is_visible('labor_policies_card'), 'Labor policies card is not found'