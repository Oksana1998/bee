from distutils.util import strtobool
from time import sleep

from behave import use_step_matcher, when, step, then
from selenium.webdriver.common.by import By
from features.lib.helpers.configurator import Configurator
from features.lib.helpers.general_utils import email_generator
from features.lib.pages import LoginPage
from features.lib.pages.modules.sidebar_menu import SideBarMenus

use_step_matcher("re")


@when('I specify (?P<user_role>.+) login')
def specify_login(context, user_role):
    page = LoginPage(context.driver)
    page.specify_login(Configurator().define_user_role(user_role))


@step('specify (?P<user_password>.+)')
def specify_password(context, user_password):
    page = LoginPage(context.driver)
    page.specify_login_password(Configurator().define_user_password(user_password))


@step('(?P<tick_me>.+) tick "Remember Me"')
def do_not_tick_remember_me(context, tick_me):
    LoginPage(context.driver).tick_remember_me(False) if 'not' in tick_me else LoginPage(
        context.driver).tick_remember_me()


@step('click "Continue" button')
def click_continue_button(context):
    LoginPage(context.driver).click_continue()


@step("proceed two factor authentication with (?P<two_factor_code>.+) code")
def proceed_2f_authentication_with_0000_code(context, two_factor_code):
    page = LoginPage(context.driver)
    page.specify_2fa_code(two_factor_code)


@step('click on "Sign in" button')
def click_sign_in_button(context):
    LoginPage(context.driver).click_sign_in_button()


@then('user is logged in')
def user_is_logged_in(context):
    page = LoginPage(context.driver)
    assert page.is_visible('successful_message'), 'Message container is not visible or absent'


@step('user can see "(?P<message_text>.+)" message')
def user_can_see_message(context, message_text):
    page = LoginPage(context.driver)
    assert page.successful_message.text == message_text, 'Successful login message is not correct'


@then("user is not logged in")
def user_is_not_logged_in(context):
    page = LoginPage(context.driver)
    assert page.is_visible('login_field'), '"Login" field is not visible or absent'
    assert page.is_visible('password_field'), '"Password" field is not visible or absent'
    assert page.is_visible('continue_button'), '"Continue" button is not visible or absent'


@step('user can see warning "(?P<message_text>.+)" message')
def user_see_warning_message(context, message_text):
    page = LoginPage(context.driver)
    page.is_visible(select_by=By.XPATH, locator='//span[text()="%s"]' % message_text)


@step("read companies list to choose for enter")
def read_companies_list(context):
    page = LoginPage(context.driver)
    response_list = page.find_elements(selector=By.XPATH, locator='//li[@class="workspaces__list-item company"]')
    companies_list = []
    for i in response_list:
        companies_list.append(i.text)
    assert companies_list is not None, 'No company presence in list'
    response_list[0].click()
