from behave import use_step_matcher, when, then, step
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from features.lib.pages import EServicesPage

use_step_matcher("re")


@then('Verify that "e-services" page has the next elements')
def verify_e_services_page_elements(context):
    page = EServicesPage(context.driver)
    assert page.is_visible('saudization_certificate'), 'saudization_certificate" card is not found'
    assert page.is_visible('change_occupation'), 'change_occupation" card is not found'
    assert page.is_visible('transferring_worker'), 'transferring_worker" card is not found'
    assert page.is_visible('working_permit'), 'working_permit" card is not found'
    assert page.is_visible('visa_issuance_service'), 'visa_issuance_service" card is not found'
    assert page.is_visible('labor_market_index'), 'labor_market_index card is not found'
    assert page.is_visible('labor_policies'), 'labor_policies card is not found'
    assert page.is_visible('national_address_registration'), 'national_address_registration is not found'
