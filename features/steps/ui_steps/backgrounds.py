from behave import use_step_matcher, given
from selenium.webdriver.common.by import By
from features.lib.pages import LoginPage, WorkspacesPage, ExpatsPage, ControlPanelPage, SignUpPage, DashboardPage,\
    EServicesPage
from features.lib.helpers.configurator import Configurator

use_step_matcher("re")


@given("I am on (?P<page_name>.+) page")
def visit_page_by_its_name(context, page_name):
    if page_name == 'Login':
        page = LoginPage(context.driver)
        page.reset_cookies()
        page.refresh_page()
        page.visit(Configurator().define_page_url(page_name))
        assert page.is_visible('login_field'), '"Login" field is not visible or absent'
        assert page.is_visible('password_field'), '"Password" field is not visible or absent'
        assert page.is_visible('continue_button'), '"Continue" button is not visible or absent'
    else:
        context.driver.get(Configurator().define_page_url(page_name))


@given('I am on "(?P<page_name>.+)" page as (?P<user_role>.+) of "(?P<company_name>.+)" company')
def visit_page_by_user_role(context, page_name, user_role, company_name):
    page_class = {
        'Login': LoginPage(context.driver),
        'Workspaces': WorkspacesPage(context.driver),
        'Expat employees': ExpatsPage(context.driver),
        'Control panel': ControlPanelPage(context.driver),
        'Sign up': SignUpPage(context.driver),
        'Dashboard': DashboardPage(context.driver),
        'E-Services': EServicesPage(context.driver)

    }
    page = page_class.get(page_name, "Invalid page name provided")
    page.visit(page.base_url)
    if LoginPage(context.driver).is_visible('login_field'):
        page = LoginPage(context.driver)
        page.specify_login(Configurator().define_user_role(user_role))
        page.specify_login_password(Configurator().default_password)
        page.click_continue()
        page.specify_2fa_code()
        page.click_sign_in_button()

        page = WorkspacesPage(context.driver)
        response_list = page.find_elements(selector=By.XPATH, locator='//li[@class="workspaces__list-item company"]')
        companies_list = []
        for i in response_list:
            companies_list.append(i.text)
        assert companies_list is not None, 'No company presence in list'
        if company_name not in companies_list:
            response_list[0].click()
        else:
            for i in response_list:
                if i.text == 'Company A':
                    i.click()
                    break
