import json
import random

from behave import use_step_matcher, when, then, step
from features.lib.helpers.api_client.api_client import Api
from features.lib.helpers.configurator import Configurator

use_step_matcher("re")


# Scenario: Login with valid login and password
@when('I post valid login and password via (?P<end_point>.+) end point')
def step_impl(context, end_point):
    api_client = Api(user_email='individual@qiwa.info', password='individual')
    context.response = api_client.login()
    context.api_client = api_client


@then('I get next response')
def step_impl(context):
    table_keys = context.table
    for row in table_keys:
        assert str(row[1]) == str(context.response.__getattribute__(row[0])), '"%s" is not %s\nGot: %s' % (
            str(row[0]), str(row[1]), context.response.__getattribute__(row[0]))


@step('proceed two-factor authentication')
def proceed_two_factor_authentication(context):
    response = context.api_client.proceed_two_factor_authentication()
    assert response.reason == 'OK' and response.status_code == 200, (
        'Two-factor authentication is not success\nError: %s' % json.loads(response.text)['errors'])


@when('I sign in as (?P<user_type>.+)')
def step_impl(context, user_type):
    api_client = Api(user_email=Configurator().define_user_role(user_type),
                     # password=Configurator().define_user_password('%s password' % user_type))
                     password=Configurator().default_password)
    api_client.login()
    context.api_company_admin_client = api_client


@step('get which companies user has in workspace')
def get_which_companies_user_has_in_workspace(context):
    user_response = json.loads(context.api_company_admin_client.get_current_session().text)
    context.current_company_id = user_response['company']['id']

    context.user_company_ids = []
    for i in user_response['user']['workspaces']:
        context.user_company_ids.append(i['company_id'])


@then('session can be patched by another company from user workspace')
def session_can_be_patched_by_another_company_from_workspace(context):
    differ_company_id = random.choice([i for i in context.user_company_ids if i not in [context.current_company_id]])

    context.api_company_admin_client.change_workspace(differ_company_id)
    user_response = json.loads(context.api_company_admin_client.get_current_session().text)
    assert user_response['company']['id'] == differ_company_id, 'Workspace was not changed.\nExpected: %s\nGot: %s' % (
        differ_company_id, user_response['company']['id'])
