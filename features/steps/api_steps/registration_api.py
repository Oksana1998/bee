import json
from time import sleep

from behave import use_step_matcher, given, then, step
from requests import ConnectionError

from features.lib.helpers.api_client.api_client import Api
from features.lib.helpers.configurator import Configurator
from features.lib.helpers.general_utils import email_generator, random_national_id

use_step_matcher("re")


@given('I create (?P<saudi_user>.+) individual user with Absher')
def new_absher_account_can_be_created(context, saudi_user):
    key_values = context.table
    national_id = int(next((c['value'] for c in key_values if 'national_id' == c[0]), 0))
    context.national_id = str(national_id) if bool(national_id) else str(random_national_id(
        saudi=False if saudi_user == 'expat' else True))
    confirm_code = str(next((c['value'] for c in key_values if 'confirmation_code' == c[0]), '0000'))
    number = str(next((c['value'] for c in key_values if 'number' == c[0]), '000000000'))
    prefix = int(next((c['value'] for c in key_values if 'prefix' == c[0]), '966'))
    user_code = str(next((c['value'] for c in key_values if 'code' == c[0]), '000000'))
    locale = str(next((c['value'] for c in key_values if 'locale' == c[0]), 'en'))
    context.user_email = email_generator()
    password = str(next((c['value'] for c in key_values if 'password' == c[0]), Configurator().default_password))
    pwd_confirm = str(next((c['value'] for c in key_values if 'password_confirmation' == c[0]
                            ), Configurator().default_password))
    print 'Generated email: %s' % context.user_email
    context.api_new_user_client = Api()

    # Checks if user exists
    search_result = context.api_admin_client.find_user_by_national_id(context.national_id)
    if search_result:
        context.api_admin_client.delete_user(search_result['id'])

    response = context.api_new_user_client.create_absher_user(
        confirmation_code=confirm_code, number=number, prefix=prefix, user_code=user_code, locale=locale,
        login=context.user_email, national_id=context.national_id, pwd=password, pwd_confirm=pwd_confirm)

    assert response.status_code == 201, (
            'User with %s email was not created\nExpected response code: 201\nActual: %s\nReason: %s\nError: %s' % (
        context.user_email, response.status_code, response.reason, json.loads(response.text)))
    assert response.reason == 'Created', 'Incorrect reason.\nExpected: Created\nActual: %s' % response.reason


@then('get notification service request to assert for "(?P<entity>.+)"')
def step_impl(context, entity):
    api_new_user_client = Api(user_email=context.user_email, password=Configurator().default_password)
    for i in xrange(10):
        try:
            api_new_user_client.login()
            api_new_user_client.proceed_two_factor_authentication()
            context.api_new_user_client = api_new_user_client
            break
        except ConnectionError:
            sleep(1)

    response = context.api_new_user_client.get_current_session()
    context.user_info = json.loads(response.text)
    context.user_ids.append(context.user_info['user']['id'])
    response = json.loads(context.api_new_user_client.get_notification_service_list().text)
    assert response['global']['code'] == 'identity-created', 'Identity record is absent'


@step("check (?P<citizen_type>.+) user retrieves info from Laborer")
def step_impl(context, citizen_type):
    context.service_response = json.loads(
        context.api_new_user_client.get_user_info(context.user_info['user']['id']).text)
    assert context.service_response['citizen_type'] == citizen_type, (
            'Citizen type is not proper\nExpected: %s\nGot: %s' % (
        citizen_type, context.service_response['citizen_type']))
    assert context.service_response['national_id'] == str(context.national_id), (
            'National ID is not correct\nExpected: %s\nGot: %s' % (
        context.national_id, context.service_response['national_id']))
    assert context.service_response['laborer_data'] is not None if citizen_type == 'saudi' else (
        'laborer_data' in context.service_response, 'Laborer data is absent')


@step("check (?P<citizen_type>.+) user retrieves info from Laborer and Working Permit service")
def step_impl(context, citizen_type):
    context.service_response = json.loads(
        context.api_new_user_client.get_user_info(context.user_info['user']['id']).text)
    assert context.service_response['citizen_type'] == citizen_type, (
            'Citizen type is not proper\nExpected: %s\nGot: %s' % (
        citizen_type, context.service_response['citizen_type']))
    assert context.service_response['national_id'] == str(context.national_id), (
            'National ID is not correct\nExpected: %s\nGot: %s' % (
        context.national_id, context.service_response['national_id']))
    assert 'work_permit' in context.service_response, 'Work permit is absent'
    assert context.service_response['laborer_data'] is not None if citizen_type == 'saudi' else (
        'laborer_data' in context.service_response, 'Laborer data is absent')


@step("check (?P<citizen_type>.+) user was added to company retrieved from Laborer service")
def check_expat_user_was_added_to_company(context, citizen_type):
    assert context.service_response['companies'] is not None if citizen_type == 'saudi' else (
        'companies' in context.service_response, 'Companies data is absent')
    assert context.service_response['company_name'] is not None if citizen_type == 'saudi' else (
        'company_name' in context.service_response, 'Company name data is absent')

