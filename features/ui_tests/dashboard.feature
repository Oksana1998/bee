@dashboard
Feature: Dashboard page
  Verifies functionality on "Dashboard" page

  Background: I am on "Dashboard" page
    Given I am on "Dashboard" page as Company admin of "Company A" company

    Scenario: Verify that "Dashboard" page has the required elements for company admin
      Then Verify that page has the next elements
        | Company profile         |
        | Commercial record       |
        | Labor index             |
        | Expat employees         |
        | Saudization certificate |
        | Mowaamah certificate    |
        | Recent activities       |
        | Reminders               |
        | Labor Policies          |