@control @regression
Feature: Control panel
  Verifies functionality on "Control Panel" page

  Background: I am on "Expat employees" page
    Given I am on "Control panel" page as Company admin of "Company A" company

    Scenario: Verify that "Control panel" page has required elements for Control panel page

      When verify "Control panel" page has the next cards
        | Roles management |
        | Product usage    |
        | Subscriptions    |
        | Invoices         |
        | Support Tickets  |
        | Switch accounts  |
      Then "Roles management" card contains the next columns order
        | column_name |
        | Role        |
        | Description |
        | Users       |
        | Actions     |
      And "Invoices" card contains the next columns
        | column_name          |
        | Product              |
        | Payment status       |
        | Invoices issues      |
        | Sadad payment number |
        | Actions              |
