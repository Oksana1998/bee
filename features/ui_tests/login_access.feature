@login @regression
Feature: Login Access
  Verifies that user can login Qiwa

  Background: Open Login page
    Given I am on Login page

    Scenario: Login as valid user
      When I specify Company admin login
      And specify Company admin password
      But do not tick "Remember Me"
      And click "Continue" button
      And proceed two factor authentication with 0000 code
      And click on "Sign in" button
      Then user is logged in
      And user can see "You have successfully logged in." message

    Scenario: Login as invalid user
      When I specify invalid user login
      And specify invalid user password
      And click "Continue" button
      Then user is not logged in
      And user can see warning "Login or password incorrect." message
