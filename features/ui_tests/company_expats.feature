@expats @regression
Feature: Expat employees page for Company admin

  Background: I am on "Expat employees" page
    Given I am on "Expat employees" page as Company admin of "Company A" company

    Scenario: Verify "Expat employees" page has required elements for Company admin
      Then verify page has the next elements
        | Expat statistics |
        | Expat management |
        | Issue or renew WP|
        | Pagination       |
      And Expat management table contains columns in next order
        | column_name                       |
        | User name                         |
        | Iqama                             |
        | Nationality                       |
        | Occupation                        |
        | Permit expiry date                |
        | Permit status                     |
        | Sadad number                      |
        | Actions                           |
