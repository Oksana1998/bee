@e_services
Feature: E-Services page
  Verifies functionality on "E-Services" page

  Background: I am on "E-Services" page
    Given I am on "E-Services" page as Company admin of "Company A" company

    Scenario: Verify that "E-Services" page has the required elements for company admin
      Then Verify that "e-services" page has the next elements
        | Saudization Certificate       |
        | Change Occupation             |
        | Transferring Worker           |
        | Issue & Renew Working Permits |
        | Visa Issuance service         |
        | Labor Market index            |
        | Labor Policies                |
        | National Address Registration |
