@sign_up @regression
Feature: Registration in Qiwa
  Verifies registration by National id or Iqama

  Background: Open Sign up page
    Given I am on Sign Up page


    Scenario: Sign up with national id
      When I specify National id number 1004081194
      And enter birthday in Hijiri
        | key   | value |
        | day   | 1     |
        | month | 1     |
        | year  | 1424  |
      And switch to the second step
      And enter Absher confirmation code 000000
      And click on "Next" button in the second step
      And enter valid email as automation_test@p2h.com
      And enter valid password as 12345678aA@
      And confirm password with 12345678aA@
      And click on "Next" button in the third step
      And enter phone number prefix 966
      And enter phone number 000000000
      And enter Qiwa confirmation code 0000
      And tick agree with "Terms and Conditions" checkbox
      And click on "Create account" button
      Then user is registered and can see "You successfully registered. You can login now." message
      And user can see "success registration" page

    Scenario: Sign up with Iqama id
      When I specify Iqama id number 2323434354
      And check that birthday fields are disabled
      And switch to the second step
      And enter Absher confirmation code 000000
      And click on "Next" button in the second step
      And enter valid email as automation_test@p2h.com
      And enter valid password as 12345678aA@
      And confirm password with 12345678aA@
      And click on "Next" button in the third step
      And enter phone number prefix 966
      And enter phone number 000000000
      And enter Qiwa confirmation code 0000
      And tick agree with "Terms and Conditions" checkbox
      And click on "Create account" button
      Then user is registered and can see "You successfully registered. You can login now." message
      And user can see "success registration" page

    Scenario: user's wrong date of birth validates by GetCitizenInfo service
      When I specify National id number 1004081195
      And enter birthday in Hijiri
        | key   | value |
        | day   | 1     |
        | month | 1     |
        | year  | 1411  |
      And click on "Verify my information" button
      Then I shouldn't see "Something went wrong. Please, try again." message
      And I should see "Your national ID or birthdate is incorrect. Please try again" message
