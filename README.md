Local config files (_for local development only_)
---
You will need to make copies for _`bahave.ini`_ and _`config.yml`_ files with local prefix inside:
* `behave.local.ini` (in "config" folder)
* `config.local.yml` (in root of project)

Setup Selenium grid
---
1. Install JRE
2. Download selenium stand alone server jar file (https://www.seleniumhq.org/download/)
3. Create folder (e.g. selenium_grid) and put the downloaded jar file into it.
4. Open 2 terminals and navigate to this folder and execute:
- for hub terminal #1: 
**`java -jar <name_of_the_file>.jar -role hub -timeout 300 -port 4445`**
- for browser node terminal #2:
 **`java -jar selenium-server-standalone-3.14.0.jar -timeout 300 -port 5556 -role node -hub http://localhost:4445/grid/register/ -browser "browserName=chrome,maxInstances=5,platform=WINDOWS,seleniumProtocol=WebDriver" -maxSession 5`**

For additional browser node open one more terminal and execute code like for terminal #2. But change _`port`_ and _`browserName`_

|attribute|description|
|:---:|:---:|
|browserName|name of desired browser: `chrome, firefox, internet explorer, MicrosoftEdge`|
|platform|planform name: `ANY, WINDOWS, UNIX, MACOS`|
|version|version of browser|

Download corresponding webdrivers for installed local browsers:

|browser|url|
|:---:|:---:|
|Google Chrome|http://chromedriver.chromium.org/|
|Mozilla Firefox|https://github.com/mozilla/geckodriver/releases|
|EDGE|https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/|
|Internet Explorer|https://www.seleniumhq.org/download/ 
| |`The Internet Explorer Driver Server` paragraph|
|Safari|DEPRECATED - use Apple's SafariDriver with Safari 10+|
Note: ports for browser nodes must be different from each other.

Command line arguments:
---
|Argument name|description|
|---|---|
|RP_PROJECT|Project name on report portal to where post test results|
|LAUNCH_NAME|Prefix of launch name|
Note: all arguments must be marked with `-D` attribute.
Example: `-D RP_PROJECT=QIWA_API -D LAUNCH_NAME="Debug_API"`

Tests pre-launch
---
Before tests run on remote machine we need to execute shell script which will modify:

_`behave.ini`_ with:
- `rp_project` with project name to where report results.
- `rp_token` with token of corresponding user.

|Project name|Description|
|:---:|:---:|
|QIWA_UI|Project for UI tests|
|QIWA_API|Project for API tests|
|QIWA_DEBUG|Project debug runs|
`sed -i "s/^rp_project =.*/rp_project = project_name/" behave.ini`

`sed -i "s/^rp_token =.*/rp_token = user_token/" behave.ini`

_`config.yml`_ with:
- `env_type` with server type.

|Server type|Value|
|:---:|:---:|
|demo|Demo server|
|staging|Staging server|
`sed -i "s/^env_type:.*/env_type: 'server_type'/" config/config.yml`

Tests launch
---
1. Open terminal
2. Navigate to source root folder of project
3. Execute `behave command_line_arguments tags`

Example: `behave -D RP_PROJECT=QIWA_API -D LAUNCH_NAME=Debug_API --tags=@api`

